import React, { Component } from 'react';
import Menu from './components/Menu';
import routes from './routes';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  showContent = (routes) => {
    let result = null;
    if (routes.length) {
      result = routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.main}
        />
      ));
    }
    return <Switch>{result}</Switch>;
  };

  render() {
    return (
      <Router>
        <Menu />
        {this.showContent(routes)}
      </Router>
    );
  }
}

export default App;
