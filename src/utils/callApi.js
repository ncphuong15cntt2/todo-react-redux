import axios from 'axios';
axios.defaults.baseURL = 'https://5ec0934225ad400016560bc9.mockapi.io/api/v1/';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded';

const callApi = (payload, method = 'GET', body = null) => {
  return axios({
    url: payload,
    method: method,
    data: body,
  })
    .then((res) => {
      if (res && res.data) {
        return res.data;
      }
      return null;
    })
    .catch((err) => {
      throw err;
    });
};

export default callApi;
