import types from '../constants/types';
import callApi from '../utils/callApi';
import api from '../constants/api';

export const getEndpoint = (endpoint, id) => endpoint.replace(':id', id);

export const callDispatch = (type, payload) => ({
  type,
  payload
});

export const fetchTasks = () => async (dispatch) => {
  try {
    const data = await callApi(api.FETCH_TASK);
    dispatch(callDispatch(types.LIST_ALL, data));
  } catch (e) {
    throw e;
  }
};

export const deleteTask = id => async (dispatch) => {
  try {
    const endpoint = getEndpoint(api.DELETE_TASK, id);
    await callApi(endpoint, 'DELETE');
    dispatch(callDispatch(types.DELETE_TASK, id));
  } catch (e) {
    throw e;
  }
};

export const updateStatus = (id, status) => async (dispatch) => {
  try {
    const endpoint = getEndpoint(api.EDIT_TASK, id);
    await callApi(endpoint, 'PUT', {
      status: !status
    });
    dispatch(callDispatch(types.UPDATE_STATUS, id));
  } catch (e) {
    throw e;
  }
};

export const saveTask = task => async (dispatch) => {
  try {
    let data = null;
    const body = {
      name: task.name,
      status: task.status
    };
    if (task.id) {
      const endpoint = getEndpoint(api.EDIT_TASK, task.id);
      data = await callApi(endpoint, 'PUT', body);
    } else {
      data = await callApi(api.ADD_TASK, 'POST', body);
    }
    dispatch(callDispatch(types.SAVE_TASK, data));
  } catch (e) {
    throw e;
  }
};

export const detailTask = id => async (dispatch) => {
  try {
    const endpoint = getEndpoint(api.EDIT_TASK, id);
    const task = await callApi(endpoint);
    dispatch(callDispatch(types.EDIT_TASK, task));
  } catch (e) {
    throw e;
  }
};

export const filterTask = filter => ({
  type: types.FILTER_TASK,
  filter
});

export const sortTask = sort => ({
  type: types.SORT_TASK,
  sort
});

export const searchTask = keySearch => ({
  type: types.SEARCH_TASK,
  keySearch
});
