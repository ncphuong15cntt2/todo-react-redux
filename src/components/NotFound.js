import React, { PureComponent } from 'react';

export default class NotFound extends PureComponent {
  render() {
    return (
      <div className="text-center">
        <strong>This page not found!</strong>
      </div>
    );
  }
}
