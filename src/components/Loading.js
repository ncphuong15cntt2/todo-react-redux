import React, { PureComponent } from 'react';
import '../assets/scss/loading.scss';

export default class Loading extends PureComponent {
  render() {
    return (
      <div className="container">
        <h5 className="text-center">Loading...</h5>
      </div>
    );
  }
}
