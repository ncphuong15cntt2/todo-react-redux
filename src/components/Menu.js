import React, { Component } from 'react';
import menus from '../constants/menu';
import MenuLink from '../constants/MenuLink';

export default class Menu extends Component {
  showMenus = (menus) => {
    let result = null;
    if (menus.length) {
      result = menus.map((menu, index) => (
        <MenuLink
          key={index}
          isExact={menu.exact}
          label={menu.name}
          to={menu.to}
        />
      ));
    }
    return result;
  };

  render() {
    return (
      <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
        <span className="navbar-brand">TODO APP</span>
        <ul className="navbar-nav">{this.showMenus(menus)}</ul>
      </nav>
    );
  }
}
