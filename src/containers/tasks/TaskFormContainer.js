import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { detailTask, saveTask } from 'actions/tasks';
import TaskForm from 'features/Task/components/TaskForm/index';

class TaskFormContainer extends PureComponent {
  render() {
    const {
      onSaveTask,
      goBack,
      onGetDetailTask,
      match,
      itemEditing
    } = this.props;

    return (
      <TaskForm
        goBack={goBack}
        itemEditing={itemEditing}
        match={match}
        onGetDetailTask={onGetDetailTask}
        onSaveTask={onSaveTask}
      >
        this.props.children
      </TaskForm>
    );
  }
}

TaskFormContainer.propTypes = {
  onSaveTask: PropTypes.func.isRequired,
  itemEditing: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.bool.isRequired
  }).isRequired,
  goBack: PropTypes.func.isRequired,
  onGetDetailTask: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  itemEditing: state.itemEditing
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onSaveTask: (task) => {
    dispatch(saveTask(task));
  },
  onGetDetailTask: (id) => {
    dispatch(detailTask(id)).catch(() => {
      ownProps.goToNotFound();
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskFormContainer);
