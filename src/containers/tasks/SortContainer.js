import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as action from '../../actions/tasks';
import TaskSort from '../../features/Task/components/TaskSort/index';
import PropTypes from 'prop-types';

class SortContainer extends Component {
  render() {
    const { onHandleSort, sort } = this.props;
    return <TaskSort onHandleSort={onHandleSort} sort={sort} />;
  }
}

SortContainer.propTypes = {
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
  }),
  onHandleSort: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  sort: state.sort,
});

const mapDispatchToProps = (dispatch, props) => ({
  onHandleSort: (sort) => {
    dispatch(action.sortTask(sort));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SortContainer);
