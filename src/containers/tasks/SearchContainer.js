import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as action from 'actions/tasks';
import Search from 'features/Task/components/Search/index';

class SearchContainer extends PureComponent {
  render() {
    const { onSearch } = this.props;
    return <Search onSearch={onSearch} />;
  }
}

SearchContainer.propTypes = {
  onSearch: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  onSearch: (keySearch) => {
    dispatch(action.searchTask(keySearch));
  }
});

export default connect(null, mapDispatchToProps)(SearchContainer);
