import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {
  deleteTask, fetchTasks, filterTask, updateStatus
} from 'actions/tasks';
import DataTable from 'features/Task/components/DataTable';
import Loading from 'components/Loading';
import TaskList from 'features/Task/components/TaskList';

const TaskItem = React.lazy(() => import('features/Task/components/TaskItem/index'));

class TaskContainer extends Component {
  componentDidMount() {
    this.props.onFetchTask();
  }

  showTasks = (tasks) => {
    const { onUpdateStatus, onDelete } = this.props;

    let result = null;
    if (tasks.length) {
      result = tasks.map((task, index) => (
        <TaskItem
          key={task.id}
          index={index}
          onDelete={onDelete}
          onUpdateStatus={onUpdateStatus}
          task={task}
        />
      ));
    }

    return result;
  };

  showDataTable = () => {
    const { onFilter } = this.props;
    return <DataTable onFilter={onFilter} />;
  };

  render() {
    let { tasks } = this.props;
    const { filter, sort, keySearch } = this.props;
    if (keySearch) {
      tasks = tasks.filter(
        el => el.name.toLowerCase().indexOf(keySearch.trim()) !== -1
      );
    }
    if (filter.filterName) {
      tasks = tasks.filter(
        el => el.name.toLowerCase().indexOf(filter.filterName.trim()) !== -1
      );
    }
    if (filter.filterStatus > -1) {
      tasks = tasks.filter(el => el.status === !!filter.filterStatus);
    }
    tasks = _.orderBy(tasks, [sort.by], sort.value ? ['asc'] : ['desc']);

    return (
      <Suspense fallback={<Loading />}>
        <TaskList>
          {this.showDataTable()}
          {this.showTasks(tasks)}
        </TaskList>
      </Suspense>
    );
  }
}

TaskContainer.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      status: PropTypes.bool.isRequired
    })
  ).isRequired,
  filter: PropTypes.shape({
    filterName: PropTypes.string.isRequired,
    filterStatus: PropTypes.number.isRequired
  }).isRequired,
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
  }).isRequired,
  keySearch: PropTypes.string.isRequired,
  onUpdateStatus: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onFilter: PropTypes.func.isRequired,
  onFetchTask: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  tasks: state.tasks,
  filter: state.dataTable,
  sort: state.sort,
  keySearch: state.search
});

const mapDispatchToProps = dispatch => ({
  onFetchTask: () => {
    dispatch(fetchTasks());
  },
  onUpdateStatus: (id, status) => {
    dispatch(updateStatus(id, status));
  },
  onDelete: (id) => {
    dispatch(deleteTask(id));
  },
  onFilter: (filter) => {
    dispatch(filterTask(filter));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskContainer);
