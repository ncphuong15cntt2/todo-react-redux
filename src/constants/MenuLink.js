import React from 'react';
import { Link, Route } from 'react-router-dom';

const MenuLink = ({ label, to, isExact }) => (
  <Route
    children={({ match }) => {
      const active = match ? 'active' : '';
      return (
        <li className={`nav-item ${active}`}>
          <Link className="nav-link" to={to}>
            {label}
          </Link>
        </li>
      );
    }}
    exact={isExact}
    path={to}
  />
);

export default MenuLink;
