const menus = [
  {
    name: 'Home',
    to: '/',
    exact: true
  },
  {
    name: 'Tasks Manager',
    to: '/task-list',
    exact: false
  }
];

export default menus;
