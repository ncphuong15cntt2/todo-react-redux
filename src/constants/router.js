const router = {
  HOME_PAGE: '/',
  TASK_PAGE: '/task-list',
  TASK_ADD: '/tasks/add',
  TASK_EDIT: '/tasks/:id/edit',
  NOT_FOUND: '',
  NOT_FOUND_PAGE: '/not-found'
};

export default router;
