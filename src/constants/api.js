const api = {
  FETCH_TASK: 'tasks',
  DELETE_TASK: 'tasks/:id',
  EDIT_TASK: 'tasks/:id',
  ADD_TASK: 'tasks'
};

export default api;
