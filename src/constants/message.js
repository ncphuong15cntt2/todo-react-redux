const messages = {
  require: '{field} is required'
};

export default messages;
