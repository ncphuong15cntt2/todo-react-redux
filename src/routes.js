import React from 'react';
import HomePage from './features/Home/pages';
import TaskPage from './features/Task/pages';
import TaskActionPage from './features/Task/pages/TaskAction';
import NotFound from './components/NotFound';
import router from './constants/router';

const routes = [
  {
    path: router.HOME_PAGE,
    exact: true,
    main: () => <HomePage />
  },
  {
    path: router.TASK_PAGE,
    exact: false,
    main: () => <TaskPage />
  },
  {
    path: router.TASK_ADD,
    exact: false,
    main: ({ history }) => <TaskActionPage history={history} />
  },
  {
    path: router.TASK_EDIT,
    exact: false,
    main: ({ match, history }) => (
      <TaskActionPage history={history} match={match} />
    )
  },
  {
    path: router.NOT_FOUND,
    exact: false,
    main: () => <NotFound />
  }
];

export default routes;
