import React, { Component } from 'react';
import TaskFormContainer from '../../../containers/tasks/TaskFormContainer';
import router from '../../../constants/router';

export default class TaskAction extends Component {
  goBack = () => {
    const { history } = this.props;
    history.push(router.TASK_PAGE);
  };

  goToNotFound = () => {
    const { history } = this.props;
    history.push(router.NOT_FOUND_PAGE);
  };

  render() {
    const { match } = this.props;

    return (
      <TaskFormContainer
        match={match}
        goBack={this.goBack}
        goToNotFound={this.goToNotFound}
      />
    );
  }
}
