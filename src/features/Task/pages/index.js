import React, { Component } from 'react';
import TaskContainer from '../../../containers/tasks/TaskContainer';
import SearchContainer from '../../../containers/tasks/SearchContainer';
import SortContainer from '../../../containers/tasks/SortContainer';
import { Link } from 'react-router-dom';
import router from '../../../constants//router';

export default class TaskPage extends Component {
  render() {
    return (
      <div className="container">
        <div className="text-center">
          <h1>Tasks Manager</h1>
          <hr />
        </div>
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <Link
              type="button"
              className="btn btn-primary"
              to={router.TASK_ADD}
            >
              <span className="fa fa-plus" /> Add new task
            </Link>
            <div className="row mt-2">
              <SearchContainer />
              <SortContainer />
            </div>
            <div className="row mt-2">
              <TaskContainer />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
