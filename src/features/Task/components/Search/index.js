import React, { Component } from 'react';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keySearch: '',
    };
  }

  handleChange = (e) => {
    this.setState({
      keySearch: e.target.value,
    });
  };

  onSearch = () => {
    this.props.onSearch(this.state.keySearch.toLowerCase().trim());
  };

  render() {
    return (
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            placeholder="Enter the key search..."
            name="keySearch"
            value={this.state.keySearch}
            onChange={this.handleChange}
          />
          <span className="input-group-append">
            <button
              className="btn btn-primary"
              type="button"
              onClick={this.onSearch}
            >
              <span className="fa fa-search" /> Search
            </button>
          </span>
        </div>
      </div>
    );
  }
}

export default Search;
