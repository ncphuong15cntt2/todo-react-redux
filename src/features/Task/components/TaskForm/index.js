import React, { Component } from 'react';
import './index.scss';
import messages from 'constants/message';

export default class TaskForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
      status: false,
      errors: {}
    };
  }

  componentDidMount() {
    const { match } = this.props;
    if (match) {
      this.props.onGetDetailTask(match.params.id);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.itemEditing) {
      const { itemEditing } = nextProps;
      this.setState({
        id: itemEditing.id,
        name: itemEditing.name,
        status: itemEditing.status
      });
    }
  }

  clickBack = () => {
    this.props.goBack();
  };

  onHandleChange = (e) => {
    const { target } = e;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.setState({
      [name]: value
    });
  };

  validateAll = () => {
    const { name } = this.state;
    if (!name) {
      this.setState({
        errors: {
          name: messages.require.replace('{field}', 'Name')
        }
      });
      return false;
    }
    return true;
  };

  onSubmit = (e) => {
    e.preventDefault();
    if (!this.validateAll()) {
      return;
    }
    this.props.onSaveTask(this.state);
    this.clickBack();
  };

  onReset = () => {
    this.setState({
      name: '',
      status: false
    });
  };

  render() {
    const { match } = this.props;
    const { name, status, errors } = this.state;

    return (
      <div className="d-flex justify-content-center mt-5">
        <div className="card w-25">
          <div className="card-header bg-warning text-white">
            <h5 className="card-title mb-0">
              {match ? 'Edit task' : 'Add task'}
              <span
                className="pointer fa fa-times-circle float-right"
                onClick={this.clickBack}
              />
            </h5>
          </div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label>Name: </label>
                <input
                  className={errors.name ? 'is-invalid form-control' : 'form-control'}
                  name="name"
                  onChange={this.onHandleChange}
                  type="text"
                  value={name}
                />
                {errors.name && <div className="invalid-feedback">{errors.name}</div>}
              </div>
              <div className="custom-control custom-checkbox">
                <input
                  checked={status}
                  className="custom-control-input"
                  id="status"
                  name="status"
                  onChange={this.onHandleChange}
                  type="checkbox"
                  value={status}
                />
                <label className="custom-control-label" htmlFor="status">
                  Active
                </label>
              </div>
              <br />
              <div className="text-center">
                <button className="btn btn-success" type="submit">
                  <span className="fa fa-plus" /> {match ? 'Save' : 'Add'}
                </button>
                &nbsp;
                <button
                  className="btn btn-danger"
                  onClick={this.onReset}
                  type="button"
                >
                  <span className="fa fa-close" /> Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
