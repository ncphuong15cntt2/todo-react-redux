import React, { Component } from 'react';
import './index.scss';
import { Link } from 'react-router-dom';
import router from '../../../../constants/router';

class TaskItem extends Component {
  onUpdateStatus = () => {
    const { id, status } = this.props.task;
    this.props.onUpdateStatus(id, status);
  };

  onDelete = () => {
    this.props.onDelete(this.props.task.id);
  };

  render() {
    const { task, index } = this.props;
    const path = router.TASK_EDIT.replace(':id', task.id);

    return (
      <tr>
        <td>{index + 1}</td>
        <td>{task.name}</td>
        <td className="text-center">
          <span
            className={
              task.status
                ? 'pointer badge badge-success'
                : 'pointer badge badge-danger'
            }
            onClick={this.onUpdateStatus}
          >
            {task.status ? 'Active' : 'Deactive'}
          </span>
        </td>
        <td className="text-center">
          <Link to={path} type="button" className="btn btn-warning">
            <span className="fa fa-pencil" /> Edit
          </Link>
          &nbsp;
          <button
            type="button"
            className="btn btn-danger"
            onClick={this.onDelete}
          >
            <span className="fa fa-trash" /> Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default TaskItem;
