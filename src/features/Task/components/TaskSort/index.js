import React, { Component } from 'react';
import './index.scss';

class TaskSort extends Component {
  handleSort = (sortBy, sortValue) => {
    this.props.onHandleSort({
      by: sortBy,
      value: sortValue,
    });
  };

  render() {
    let { sort } = this.props;

    return (
      <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div className="dropdown">
          <button
            className="btn btn-primary dropdown-toggle"
            type="button"
            id="dropdownMenu1"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="true"
          >
            Sort <span className="fa fa-caret-square-o-down" />
          </button>
          <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li onClick={() => this.handleSort('name', 1)}>
              <p
                className={
                  sort.by === 'name' && sort.value
                    ? 'dropdown-item mb-0 sort_selected'
                    : 'dropdown-item mb-0'
                }
                role="button"
              >
                <span className="fa fa-sort-alpha-asc"> Name A-Z</span>
              </p>
            </li>
            <li onClick={() => this.handleSort('name', 0)}>
              <p
                className={
                  sort.by === 'name' && !sort.value
                    ? 'dropdown-item mb-0 sort_selected'
                    : 'dropdown-item mb-0'
                }
                role="button"
              >
                <span className="fa fa-sort-alpha-desc"> Name Z-A</span>
              </p>
            </li>
            <hr />
            <li onClick={() => this.handleSort('status', 0)}>
              <p
                className={
                  sort.by === 'status' && !sort.value
                    ? 'dropdown-item mb-0 sort_selected'
                    : 'dropdown-item mb-0'
                }
                role="button"
              >
                Active
              </p>
            </li>
            <li onClick={() => this.handleSort('status', 1)}>
              <p
                className={
                  sort.by === 'status' && sort.value
                    ? 'dropdown-item mb-0 sort_selected'
                    : 'dropdown-item mb-0'
                }
                role="button"
              >
                Deactive
              </p>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default TaskSort;
