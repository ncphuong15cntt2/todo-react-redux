import React, { Component } from 'react';

class DataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterName: '',
      filterStatus: -1
    };
  }

  onFilter = (e) => {
    const { target } = e;
    const { name } = target;
    const { value } = target;
    this.setState({
      [name]: value
    });
    this.props.onFilter({
      filterName:
        name === 'filterName' ? value.toLowerCase() : this.state.filterName,
      filterStatus: name === 'filterStatus' ? +value : +this.state.filterStatus
    });
  };

  render() {
    const { filterName, filterStatus } = this.state;
    return (
      <tr>
        <td />
        <td>
          <input
            className="form-control"
            name="filterName"
            onChange={this.onFilter}
            type="text"
            value={filterName}
          />
        </td>
        <td>
          <select
            className="form-control"
            name="filterStatus"
            onChange={this.onFilter}
            value={filterStatus}
          >
            <option value="-1">All</option>
            <option value="0">Deactive</option>
            <option value="1">Active</option>
          </select>
        </td>
        <td />
      </tr>
    );
  }
}

export default DataTable;
