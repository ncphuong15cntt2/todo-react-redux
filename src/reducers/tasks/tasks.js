import types from '../../constants/types';

const initialState = [];

const myReducers = (state = initialState, action) => {
  let index = -1;
  let tasksClone = [...state];

  switch (action.type) {
    case types.LIST_ALL:
      state = action.payload;
      return [...state];

    case types.SAVE_TASK:
      const task = { ...action.payload };
      index = tasksClone.findIndex((el) => el.id === task.id);
      if (index < 0) {
        tasksClone.push(task);
      } else {
        tasksClone[index] = task;
      }
      return tasksClone;

    case types.UPDATE_STATUS:
      index = tasksClone.findIndex((el) => el.id === action.payload);
      tasksClone[index] = {
        ...tasksClone[index],
        status: !tasksClone[index].status,
      };
      return tasksClone;

    case types.DELETE_TASK:
      index = tasksClone.findIndex((el) => el.id === action.payload);
      tasksClone.splice(index, 1);
      return tasksClone;

    default:
      return state;
  }
};

export default myReducers;
