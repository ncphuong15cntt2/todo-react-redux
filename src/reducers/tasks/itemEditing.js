import types from '../../constants/types';

const initialState = {
  id: '',
  name: '',
  status: false,
};
const myReducers = (state = initialState, action) => {
  switch (action.type) {
    case types.EDIT_TASK:
      return action.payload;

    case types.CLEAR_TASK:
      return initialState;

    default:
      return state;
  }
};

export default myReducers;
