import types from '../../constants/types';

const initialState = {
  by: 'name',
  value: 1,
};

const myReducers = (state = initialState, action) => {
  switch (action.type) {
    case types.SORT_TASK:
      return action.sort;

    default:
      return state;
  }
};

export default myReducers;
