import types from '../../constants/types';

const initialState = {
  filterName: '',
  filterStatus: -1,
};

const myReducers = (state = initialState, action) => {
  switch (action.type) {
    case types.FILTER_TASK:
      return action.filter;
    default:
      return state;
  }
};

export default myReducers;
