import types from '../../constants/types';

const initialState = '';
const myReducers = (state = initialState, action) => {
  switch (action.type) {
    case types.SEARCH_TASK:
      return action.keySearch;
    default:
      return state;
  }
};

export default myReducers;
