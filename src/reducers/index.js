import { combineReducers } from 'redux';
import tasks from './tasks/tasks';
import dataTable from './tasks/dataTable';
import itemEditing from './tasks/itemEditing';
import sort from './tasks/sort';
import search from './tasks/search';

const myReducers = combineReducers({
  tasks,
  dataTable,
  itemEditing,
  sort,
  search,
});

export default myReducers;
